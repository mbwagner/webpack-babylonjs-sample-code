This is a stripped-down sample on how to use [Babylon.js](https://babylon.js/) together with [Cannon.js](https://schteppe.github.io/cannon.js/) in a webpack project.

## Installation

* Clone the git repository
* In the directory, run:
`npm install &&
npm start`

And there you go!
