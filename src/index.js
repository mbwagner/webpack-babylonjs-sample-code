import {
    Engine
} from "@babylonjs/core/Engines/engine";
import {
    Scene,
} from "@babylonjs/core/scene";
import {
    ActionManager,
} from "@babylonjs/core/Actions/actionManager";
import {
    ExecuteCodeAction
} from "@babylonjs/core/Actions";
import {
    Vector3,
    Axis,
    Space
} from "@babylonjs/core/Maths/math";
import {
    FreeCamera
} from "@babylonjs/core/Cameras/freeCamera";
import {
    SpotLight
} from "@babylonjs/core/Lights/spotLight";
import {
    HemisphericLight
} from "@babylonjs/core/Lights/hemisphericLight";
import {
    Mesh
} from "@babylonjs/core/Meshes/mesh";

import {
    GridMaterial
} from "@babylonjs/materials/grid";
import {
    SceneLoader
} from "@babylonjs/core/Loading/sceneLoader";
import {
  Color3,
  TransformNode
} from "@babylonjs/core";

import * as cannon from "cannon";
import { CannonJSPlugin } from "babylonjs";

// Required side effects to populate the Create methods on the mesh class. Without this, the bundle would be smaller but the createXXX methods from mesh would not be accessible.
import "@babylonjs/core/Meshes/meshBuilder";

// Side effect for physics
import "@babylonjs/core/Physics/physicsEngineComponent";

// Get the canvas element from the DOM.
const canvas = document.getElementById("renderCanvas");

// Associate a Babylon Engine to it.
const engine = new Engine(canvas);

// Create our first scene.
var scene = new Scene(engine);
scene.clearColor = new Color3(0, 0, 0.1);

scene.enablePhysics(null, new CannonJSPlugin(true, 10, cannon));

// This creates and positions a free camera (non-mesh)
var camera = new FreeCamera("camera1", new Vector3(0, 5, -10), scene);

// This targets the camera to scene origin
camera.setTarget(Vector3.Zero());

// This attaches the camera to the canvas
camera.attachControl(canvas, true);

var light = new HemisphericLight("light1", new Vector3(0, 1, 0), scene);

// Create a grid material
var material = new GridMaterial("grid", scene);

// Our built-in 'sphere' shape. Params: name, subdivs, size, scene
var sphere = Mesh.CreateSphere("sphere1", 16, 2, scene);

// Move the sphere upward 1/2 its height
sphere.position.y = 2;

// Affect a material
sphere.material = material;

// Render every frame
engine.runRenderLoop(() => {
  scene.render();
});
